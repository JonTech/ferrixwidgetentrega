package com.example.ferrixwidget;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class SQLiteHandler extends SQLiteOpenHelper {

    public final String sqlCreateAlumni = "CREATE TABLE alumnos (ID INTEGER, nombre TEXT, grupo TEXT)";
    public final String sqlCreateDAM2 = "CREATE TABLE dam2 (codigo TEXT, className TEXT, profe TEXT, dia TEXT, hora TEXT)";

    public SQLiteHandler(@Nullable Context context,
                         @Nullable String name,
                         @Nullable SQLiteDatabase.CursorFactory factory,
                         int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("[INFO]", "Creating database");
        String SQLsentence;
        db.execSQL(sqlCreateAlumni);
        db.execSQL(sqlCreateDAM2);

        //Lunes
        db.execSQL("INSERT INTO dam2 VALUES(\"DUAL\", \"DUAL\", \"Andres Prieto\", \"Lunes\", \"15:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M5-M8\", \"Mobile Apps\", \"Lluis Perpinyà\", \"Lunes\", \"17:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M10\", \"CRM\", \"Andres Prieto\", \"Lunes\", \"18:20\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M10\", \"CRM\", \"Andres Prieto\", \"Lunes\", \"19:20\");");

        //Martes
        db.execSQL("INSERT INTO dam2 VALUES(\"DUAL\", \"DUAL\", \"Leo\", \"Martes\", \"15:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"DUAL\", \"DUAL\", \"Leo\", \"Martes\", \"16:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M9\", \"MultiThread\", \"Marta Plana\", \"Martes\", \"17:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M9\", \"MultiThread\", \"Marta Plana\", \"Martes\", \"18:20\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M5-M8\", \"Mobile Apps\", \"Lluis Perpinyà\", \"Martes\", \"19:20\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M5-M8\", \"Mobile Apps\", \"Lluis Perpinyà\", \"Martes\", \"20:20\");");

        //Miercoles
        db.execSQL("INSERT INTO dam2 VALUES(\"DUAL\", \"DUAL\", \"Leo\", \"Miercoles\", \"16:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"DUAL\", \"DUAL\", \"Leo\", \"Miercoles\", \"17:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"TUT\", \"Tutoria\", \"josefa\", \"Miercoles\", \"18:20\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M3\", \"Programacion Basica\", \"Josefa\", \"Miercoles\", \"19:20\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M5-M8\", \"Mobile Apps\", \"Lluis Perpinyà\", \"Miercoles\", \"20:20\");");

        //Jueves
        db.execSQL("INSERT INTO dam2 VALUES(\"DUAL\", \"DUAL\", \"Leo\", \"Jueves\", \"15:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"DUAL\", \"DUAL\", \"Leo\", \"Jueves\", \"16:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M7\", \"Desarrollo de Interficies\", \"Raul Vallez\", \"Jueves\", \"17:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M7\", \"Desarrollo de Interficies\", \"Raul Vallez\", \"Jueves\", \"18:20\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M3\", \"Programacion Basica\", \"Josefa\", \"Jueves\", \"19:20\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M3\", \"Programacion Basica\", \"Josefa\", \"Jueves\", \"20:20\");");

        //Viernes
        db.execSQL("INSERT INTO dam2 VALUES(\"M3\", \"Programacion Basica\", \"Josefa\", \"Viernes\", \"15:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M3\", \"Programacion Basica\", \"Josefa\", \"Viernes\", \"16:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M6\", \"Acceso a Datos\", \"Raul Vallez\", \"Viernes\", \"17:00\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M6\", \"Acceso a Datos\", \"Raul Vallez\", \"Viernes\", \"18:20\");");
        db.execSQL("INSERT INTO dam2 VALUES(\"M6\", \"Acceso a Datos\", \"Raul Vallez\", \"Viernes\", \"19:20\");");

        Log.i("[INFO]", "Finished database");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //WHAT'S ALL THAT SHIT??
        if (oldVersion == 1 && newVersion ==2){
            db.execSQL ("CREATE TABLE PERFIL (ID_PERFIL INTEGER, ID_USUARIO INTEGER, NOMBRE TEXT)");
        }

        else if (oldVersion ==2 && newVersion ==1){
            db.execSQL ("DROP TABLE IF EXISTS PERFIL");

        }
    }
}
