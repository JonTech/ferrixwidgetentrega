package com.example.ferrixwidget;

import androidx.appcompat.app.AppCompatActivity;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.sql.SQLOutput;

/*
* Esta es la clase de configuración del widget, cada vez que se invoque deberá aparecer primero
* luego llamar al widget y pasarle los parametros
* */
public class WidgetConfig extends AppCompatActivity {

    private int widgetId = 0;

    private EditText nameInput;
    private EditText grupoInput;
    private Button loginBtn;
    private Button exitBtn;

    private SQLiteHandler dbHandler;
    private SQLiteDatabase db;


    //Test Variables
    private String className = "Sin clase";
    private String profName = "Sin profe";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget_config);


        //Generating Databses
        Log.i("[INFO]", "Loading database");
        dbHandler = new SQLiteHandler(this, "DBAlumni", null, 1);
        db = dbHandler.getWritableDatabase();
        Log.i("[INFO]", "Database loaded");

        String time = FerrixWidget.getTime().split(" ")[1].split(":")[0];
        System.out.println("SHOWUMEWHATUGOT: " + time);

        //Hacemos la query aqui
        String varname1 = "SELECT * FROM dam2 WHERE dia = 'Lunes' AND hora LIKE '"+time+"%'";
        Log.i("[INFO]", "Query was: " + varname1);
        Cursor c = db.rawQuery(varname1, null);
        c.moveToFirst(); //Me parece una mierda esto del cursor

        //Ahora extraemos los resultados de nuestra queary
        try {
            className = c.getString(1);
            profName = c.getString(2);
        } catch (RuntimeException e) {
            Log.i("[INFO]", "Query returned empty");
        }



        Log.i("[INFO]", "Configurating Widget with parameters from Config Screen");
        //Resto de configuracion del Widget
        Intent intentOrigen = getIntent();
        Bundle params = intentOrigen.getExtras();

        widgetId = params.getInt(
                AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);

        System.out.println("[INFO] Widget ID is: " + widgetId);

        setResult(RESULT_CANCELED);

        //Recuperamos las referencias a los elementos útiles del layout
        nameInput = findViewById(R.id.nameInput);
        grupoInput = findViewById(R.id.grupoInput);
        loginBtn = findViewById(R.id.loginBtn);
        exitBtn = findViewById(R.id.exitBtn);

        //Implementamos el boton del Exit
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("[INFO] Program has finished");
                finish(); //Devolvemos el resultado default RESULT_CANCELED


            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences prefs = getSharedPreferences("WidgetPrefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                //Change start
                editor.putString("fw_" + widgetId + "_name", nameInput.getText().toString());
                editor.putString("fw_" + widgetId + "_grupo", grupoInput.getText().toString());
                editor.putString("fw_" + widgetId + "_class", className);
                editor.putString("fw_" + widgetId + "_prof", profName);
                //End change
                //editor.putString("msg_" + widgetId, nameInput.getText().toString()); //???
                editor.commit();

                //Actualizar el widget tras la config
                System.out.println("[INFO] Getting instance of WidgetConfig");

                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(WidgetConfig.this);
                System.out.println("[INFO] Updating the widget");
                FerrixWidget.updateAppWidget(WidgetConfig.this, appWidgetManager, widgetId);


                //Devolver un buen OK
                Intent resultado = new Intent();
                resultado.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
                //System.out.println("[CONFIG INFO] Name text is: " + nameInput.getText().toString());
                resultado.putExtra("Name", nameInput.getText().toString());
                setResult(RESULT_OK, resultado);
                finish();

                System.out.println("[INFO] OK devuelto!");
            }
        });

        Log.i("[INFO]", "Configuration ended");
    }
}
